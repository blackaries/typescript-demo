interface IStruct {
    length: number
}

function test<T extends IStruct>(arg: T): T {
    console.log(arg.length);
    return arg
}

test([1, 2, 3, 4])
test("12345678");
test({ length: 8, type: "ceshi" })


class Queue<T> {
    private data: T[];
    push(item: T) {
        return this.data.push(item)
    }
    pop(item: T) {
        return this.data.shift()
    }
}

let queue = new Queue<string>();
queue.push("123");
console.log(queue);