type IOperation = "plus" | "minus" | "divide" | "multi"
// type ICaculator = (operation: IOperation, nums: number[]) => number

interface ICaculator {
    public (operate: IOperation, numbers: number[]) : number
    plus: (numbers: number[]) => number 
    minus: (numbers: number[]) => number
}

declare const ICaculator; // 导出接口

class Caculator implements ICaculator {

    public (operate: IOperation, numbers: number[]): number {
        switch (operate) {
            case "plus":
                return this.plus(numbers);
            case "minus":
                return this.minus(numbers);
            // 这里可以继续添加其他操作的处理逻辑
            default:
                throw new Error('Unsupported operation');
        }
    }

    // 实现plus方法
    public plus(numbers: number[]): number {
        return numbers.reduce((acc, num) => acc + num, 0);
    }

    // 实现minus方法
    public minus(numbers: number[]): number {
        // 这里假设minus是对第一个数与其他数相减的结果
        return numbers.reduce((acc, num) => acc - num);
    }
}

let caculator = new Caculator();
caculator.plus([1,3,4]);